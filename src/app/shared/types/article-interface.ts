import { Profile } from './profile';

export interface ArticleInterface {
  author: Profile;
  bio: null;
  following: boolean;
  image: string;
  username: string;
  body: string;
  createdAt: string;
  description: string;
  favorited: boolean;
  favoritesCount: number;
  slug: string;
  tagList: string[];
  title: string;
  updatedAt: string;
}
