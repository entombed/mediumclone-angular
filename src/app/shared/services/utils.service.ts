import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root',
})
export class UtilsService {
  constructor() {}

  range(start: number, end: number) {
    return [...Array(end).keys()].map((item) => item + 1);
  }
}
