import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'mc-banner',
  templateUrl: './banner.component.html',
  styleUrls: ['./banner.component.scss'],
})
export class BannerComponent implements OnInit {
  title = 'Medium Clone';
  titleText = 'A place to share knowledge';
  constructor() {}

  ngOnInit() {}
}
