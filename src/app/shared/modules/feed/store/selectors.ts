import { createFeatureSelector, createSelector } from '@ngrx/store';
import { AppStateInterface } from 'src/app/shared/types/app-state-interface';
import { FeedStateInterface } from '../types/feed-state-interface';

export const feedFeaturesSelector = createFeatureSelector<
  AppStateInterface,
  FeedStateInterface
>('feed');

export const isLoadingSelector = createSelector(
  feedFeaturesSelector,
  (feedState: FeedStateInterface) => feedState.isLoading
);

export const isErrorSelector = createSelector(
  feedFeaturesSelector,
  (feedState: FeedStateInterface) => feedState.error
);

export const feedSelector = createSelector(
  feedFeaturesSelector,
  (feedState: FeedStateInterface) => feedState.date
);
