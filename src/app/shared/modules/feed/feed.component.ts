import { Component, Input, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { select, Store } from '@ngrx/store';
import { Observable, Subscription } from 'rxjs';
import { environment } from 'src/environments/environment';
import { getFeedAction } from './store/actions/get-feed-action';
import {
  feedSelector,
  isErrorSelector,
  isLoadingSelector,
} from './store/selectors';
import { GetFeedResponseInterface } from './types/get-feed-response.interface';
import { parseUrl, stringify } from 'query-string';
import { off } from 'process';

@Component({
  selector: 'mc-feed',
  templateUrl: './feed.component.html',
  styleUrls: ['./feed.component.scss'],
})
export class FeedComponent implements OnInit, OnDestroy {
  @Input('apiUrl') apiUrlProps: string;
  private subscription: Subscription = new Subscription();
  isLoading$: Observable<boolean>;
  error$: Observable<string | null>;
  feed$: Observable<GetFeedResponseInterface | null>;
  limit: number = environment.limit;
  baseUrl: string;
  currentPage;
  constructor(
    private store: Store,
    private router: Router,
    private route: ActivatedRoute
  ) {}

  ngOnInit(): void {
    this.initializeValue();
    this.initializeLiseners();
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

  initializeLiseners(): void {
    this.subscription.add(
      this.route.queryParams.subscribe((params: Params) => {
        this.currentPage = Number(params?.page ?? 1);
        this.fetchDate();
      })
    );
  }

  initializeValue(): void {
    this.isLoading$ = this.store.pipe(select(isLoadingSelector));
    this.error$ = this.store.pipe(select(isErrorSelector));
    this.feed$ = this.store.pipe(select(feedSelector));
    this.baseUrl = this.router.url.split('?')[0];
  }

  fetchDate(): void {
    const offset = this.currentPage * this.limit - this.limit;
    const parsedUrl = parseUrl(this.apiUrlProps);
    const stringifiedParams = stringify({
      limit: this.limit,
      offset: offset,
      ...parsedUrl.query,
    });
    const apiUrlWithParams = `${parsedUrl.url}?${stringifiedParams}`;
    this.store.dispatch(getFeedAction({ url: apiUrlWithParams }));
  }
}
