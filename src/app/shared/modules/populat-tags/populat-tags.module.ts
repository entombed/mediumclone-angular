import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PopulatTagsComponent } from './populat-tags.component';
import { PopularTagsService } from './services/popular-tags.service';
import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';
import { GetPopularTagsEffect } from './store/effects/get-popular-tags-effect';
import { RouterModule } from '@angular/router';
import { LoadingModule } from '../loading/loading.module';
import { ErrorMessageModule } from '../error-message/error-message.module';
import { reducers } from './store/reducers';

@NgModule({
  imports: [
    CommonModule,
    RouterModule,
    StoreModule.forFeature('popularTags', reducers),
    EffectsModule.forFeature([GetPopularTagsEffect]),
    LoadingModule,
    ErrorMessageModule,
  ],
  declarations: [PopulatTagsComponent],
  exports: [PopulatTagsComponent],
  providers: [PopularTagsService],
})
export class PopulatTagsModule {}
