import { Component, OnInit } from '@angular/core';
import { select, Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { PopularTagType } from '../../types/popularTag.type';

import {
  getPopularTagsAction,
  getPopularTagsSuccessAction,
} from './store/actions/gat-popular-tags.action';
import {
  errorSelector,
  isLoadingSelector,
  popularTagsSelector,
} from './store/selectors';

@Component({
  selector: 'mc-populat-tags',
  templateUrl: './populat-tags.component.html',
  styleUrls: ['./populat-tags.component.scss'],
})
export class PopulatTagsComponent implements OnInit {
  popularTags$: Observable<PopularTagType[] | null>;
  isLoading$: Observable<boolean>;
  error$: Observable<string>;

  constructor(private store: Store) {}

  ngOnInit() {
    this.initializeValues();
    this.fetchData();
  }

  initializeValues() {
    this.popularTags$ = this.store.pipe(select(popularTagsSelector));
    this.isLoading$ = this.store.pipe(select(isLoadingSelector));
    this.error$ = this.store.pipe(select(errorSelector));
  }
  fetchData() {
    this.store.dispatch(getPopularTagsAction());
  }
}
