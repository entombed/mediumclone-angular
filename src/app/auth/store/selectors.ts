import { createFeatureSelector, createSelector } from '@ngrx/store';
import { AppStateInterface } from 'src/app/shared/types/app-state-interface';
import { AuthStateInterface } from '../types/auth-state-interface';

export const authFeaturesSelector = createFeatureSelector<
  AppStateInterface,
  AuthStateInterface
>('auth');

export const isSubmittingSelector = createSelector(
  authFeaturesSelector,
  (authState: AuthStateInterface) => authState.isSubmitting
);

export const validationErrorsSelector = createSelector(
  authFeaturesSelector,
  (authState: AuthStateInterface) => authState.validaionErrors
);

export const isLoggedInSelector = createSelector(
  authFeaturesSelector,
  (authState: AuthStateInterface) => authState.isLoggedIn
);

export const isAnonymouseSelector = createSelector(
  authFeaturesSelector,
  (authState: AuthStateInterface) => authState.isLoggedIn === false
);

export const currentUserSelector = createSelector(
  authFeaturesSelector,
  (authState: AuthStateInterface) => authState.currentUser
);
